const conectionDB = require('../../config/DBconect');

module.exports = app => {
    const conection = conectionDB();


    //mostrar datos en la interfaz de lista-productos
    app.get('/admin/lista-productos', (req, res) =>{
        conection.query(`
            SELECT
                menu_id as id,
                menu_name as nombre,
                menu_precio as precio,
                cat_nombre as categoria
            FROM producto_menu
            INNER JOIN categoria on categoria.cat_id = producto_menu.menu_cat_id
            GROUP BY id
        `, (error, response) => {

            if(error) throw error;
            conection.query(`
                SELECT
                    cat_id as id,
                    cat_nombre as nombre
                FROM categoria ORDER BY id ASC
            `, (error, categorias) => {

                if(error) throw error;
                conection.query(`
                    SELECT
                        subcat_id as id,
                        subcat_nombre as nombre
                    FROM subcategoria ORDER BY id ASC
                `, (error, subcategoria) =>{
                    if(error) throw error;
                    
                    res.render('../views/productos', {
                        products : response,
                        categorias : categorias,
                        subcategorias : subcategoria
                    });
                })
            })

        });
    });


    //guardar un nuevo producto en la tabla producto_menu de la DDBB
    app.post('/add-producto', async (req, response) => {
        const { nameProduct, slcCatProduct, slcSubcatProduct, descripProduct, priceProduct } = req.body;
        conection.query('INSERT INTO producto_menu SET?', {
            menu_name: nameProduct,
            menu_cat_id: slcCatProduct,
            menu_subcat_id: slcSubcatProduct,
            menu_descripcion: descripProduct,
            menu_precio: priceProduct
        }, async (error, result) => {
            if (error) {
                response.send('Error al agregar un producto');
            } else {
                response.send('Producto Ingresado correctamente');
            }
        })
    });
}