const conectionDB = require('../../config/DBconect');
const bcrypt = require('bcryptjs');
const informacionFooter = require('../../inforDetalles.json')


module.exports = app => {
    const connection = conectionDB();

    app.get('/', (req, res) =>{
        connection.query('SELECT * FROM usuario', (error, response) =>{
            res.render('../views/index',{
                detalle: informacionFooter
            });
        })
    });

    app.get('/admin/usuarios', (req, res) =>{
        connection.query('SELECT * FROM usuario', (error, response) =>{
            res.render('../views/usuarios', {
                users : response      
            });
        })
    });

    //metodo para registro de usuarios
    app.post('/registerUser', async (req, response)=>{
        console.log(req.body)
        const {name, apellido, dateNac, emailUser, telef, firstPass} = req.body;
        let encripPass = await bcrypt.hash(firstPass, 8);
        connection.query('INSERT INTO usuario SET?', {
            Cliente_nombre: name,
            Cliente_apellido : apellido,
            Cliente_fechaNac : dateNac,
            Cliente_telefono : telef,
            Cliente_correo : emailUser,
            Cliente_contraseña : encripPass
        }, async (error, result) =>{
            if(error){
                console.log(error);
            }else{
                response.redirect('/');
            }
        })
    })

    //metodo para login
    app.post('/send', async (req, res)=>{
        const {emailUser, firstPass} = req.body;
        if(emailUser && firstPass){
            connection.query('SELECT * FROM usuario WHERE Cliente_correo = ?', [emailUser], async (error, response)=>{
                if(response.length == 0 || !(await bcrypt.compare( firstPass, response[0].Cliente_contraseña))){
                    res.send("El usuario o contraseña que ingreso no son correctas");
                }else{
                    //mostrar o redigir al index de usuario registrado
                    res.send('Login correcto, por el momento no tenemos que mostrarte. ¡Gracias XD!');
                }
            });
        }
    })
}

