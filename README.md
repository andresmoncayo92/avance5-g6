# Avance 4 - Grupo 6
## La Parrilla de Luchin

## Integrantes:
- Leonel Anchundia Leonel
- Delgado Alonso Pablo
- Espinoza López Eddy
- Mendoza Pilligua Jonathan
- Moncayo Zambrano Andrés **(Lider)**


### Base de datos
- **Nombre de la Base de datos:** "database_luchin"
- **Contraseña:** Sin contraseña
- **user:** root
  - ### Crear la tabla de usuario:
  ```
    CREATE TABLE Usuario (
        Cliente_id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        Cliente_nombre VARCHAR(30) NOT NULL,
        Cliente_apellido VARCHAR(30) NOT NULL,
        Cliente_fechaNac DATE,
        Cliente_telefono CHARACTER(10),
        Cliente_correo CHARACTER(50),
        Cliente_contraseña CHARACTER(150)
    );
    ```
) 

### PUERTO ***3002***

### Ejecutar por npm
- Con la terminal Prompt abierta en el mismo directorio del proyecto
- Ejecutar el comando: ***npm start***
- 
- 